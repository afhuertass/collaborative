// coleccion para guardar los cursos
Cursos = new Mongo.Collection('cursos');

// coleccion para guardar los profesores
Profesores = new Mongo.Collection('profesores');
// coleccion para guardar los materiales de cada curso.
Materiales = new Mongo.Collection('materiales');
