// Codigo para controlar el submitin


Template.viewCursoMaterialSubmit.events({

    'submit form': function(e){
	e.preventDefault();
	var id =   $(e.target).find('[name=idCurso]').val();
	var material = {
	    name: $(e.target).find('[name=nombre-material]').val() ,
	    description: $(e.target).find('[name=descripcion-material]').val(),
	    _idCurso : id
	};
	 // material listo

	material._id = Materiales.insert(material);
	Router.go('viewCursoMaterial' ,  { _id: id })

    }
});
