// codigo para el submithing del profesor.

Template.viewCursoProfesorSubmit.events({
    'submit form': function(e){
	e.preventDefault();
	var id = $(e.target).find('[name=idCurso]').val();
	var profesor = {
	    name : $(e.target).find('[name=nombre-profesor]').val(),
	    descripcion : $(e.target).find('[name=descripcion-profesor]').val(),
	    _idCurso : id
	};
	
	p = Profesores.insert(profesor);

	Router.go('viewCursoProfesores',  { _id : id});
	
    }
    
});
