/*
Archivo para configurar las rutas de la aplicacion
*/

Router.configure({
    layoutTemplate: 'layout'
    
});
Router.map( function(){
   // this.route('home' , {path: '/' });
    
    this.route('viewCursos' , {path : '/'} );
   
    this.route('viewCurso' , 
	       {
		   path:'/cursos/:_id',
		   data: function() { return Cursos.findOne(this.params._id ); }
	       });
    this.route('viewCursoMaterial' ,
	       {
		   path:'/cursos/:_id/material' ,
		   data :  function(){ 
		       data = {
			   curso : Cursos.findOne(this.params._id),
			   materiales : Materiales.find(  {_idCurso: this.params._id }) 
		       };
		       return data;
		   } ,
		   
	       });
    this.route('viewCursoMaterialSubmit' , 
	       {
		   path: '/cursos/:_id/submitm', 
		   data : function(){ return Cursos.findOne(this.params._id); }

	       });
    this.route('viewCursoProfesorSubmit' , 
	       {
		   path: '/cursos/:_id/submitp' ,
		   data : function() { return Cursos.findOne(this.params._id); }
	      });
    this.route('viewCursoProfesores' ,
	       {
		   path:'/cursos/:_id/profesors',
		   data : function(){ 
		       data = {
			   curso: Cursos.findOne(this.params._id),
			   profesores : Profesores.find( {_idCurso:this.params._id } )
		       };
		       return data; 
		   }

	       }
	      ); 
    this.route('viewCursoProfesor' , 
	       {
		   path: '/cursos/:_id/profesors/:_idProfesor'
	       }		   
	      );
    this.route('viewCursoReviews' , 
	       {
		   path: '/cursos/:_id/reviews', 
		   data : function(){ return Cursos.findOne(this.params._id); }

	       }
	      )
   
});
